# Using Onionspray

## Creating a project

Okay, there are two ways to create your own project:

1. With automatically-generated addresses.
2. With manually-mined addresses.

### With automatically-generated Onions Service addresses

Create a config file with a `.tconf` suffix -- we'll pretend it's `foo.tconf`
-- and use this kind of syntax:

    set project myproject
    hardmap %NEW_V3_ONION% foo.com
    hardmap %NEW_V3_ONION% foo.co.uk
    hardmap %NEW_V3_ONION% foo.de

... and then run

    ./onionspray config foo.tconf

... which will create the onions for you and will populate a `foo.conf` for
you, and it will then configure Onionspray from *that*.  You should probably
*delete* `foo.tconf` afterwards, since forcibly reusing it would trash your
existing onions.

### With manually-mined Onion Service addresses

#### Generating the key

* With `onionspray genkey`:
    * Run `onionspray genkey` -- it will print the name of the onion it generates.
    * Run it as many times as you wish/need.

* Alternately get a tool like [mkp224o][], [Onionmine][], [oniongen-go][], or
  [oniongen-rs][] and use that to "mine" a desirable .onion address:
    * [mkp224o][]:
        * Written in C.
        * For CPUs.
        * Requires a UNIX-like platform, like Linux, *BSD, or Cygwin.
    * [Onionmine][]:
        * A wrapper around [mkp224o][].
        * Also aids the process of getting CA-signed TLS certificates.
    * [oniongen-go][]:
        * Written in Go.
        * For CPUs.
        * Works on Linux and Windows, uses regex.
    * [oniongen-rs][]:
        * Written in Rust.
        * For CPUs.
        * Works similarly to `oniongen-go`, but is written in Rust.
    * Be sure to store your mined keys in `secrets.d`,
      naming the keys like
      `someverylongonionaddressinvolvingalotofbase23characterss.v3pub.key` and
      `someverylongonionaddressinvolvingalotofbase32characterss.v3sec.key`

Check also the [mining tips](mining.md).

[mkp224o]: https://github.com/cathugger/mkp224o
[Onionmine]: https://gitlab.torproject.org/tpo/onion-services/onionmine/
[oniongen-go]: https://github.com/rdkr/oniongen-go
[oniongen-rs]: https://gitlab.com/iamawacko-oss/oniongen-rs

#### Creating the config file

* Create a config file with a `.conf` suffix - we'll pretend it's
  `foo.conf` - and use this kind of syntax, substituting
  `a2s3c4d5e6f7g8h9` for the onion address that you generated.

    set project myproject
    hardmap z2walkyky3zzv52g3gedrhxa665qdrslyvrljg5wppibx34olslnxgqd foo.com

... and then (**IMPORTANT**) run:

    ./onionspray config foo.conf

... which will configure Onionspray.

## Starting a project

Like this:

    ./onionspray start myproject

## Subdomains management

### Overview

Subdomains are supported like this, for `dev` as an example:

    set project myproject
    hardmap z2walkyky3zzv52g3gedrhxa665qdrslyvrljg5wppibx34olslnxgqd foo.com dev

... and if you have multiple subdomains:

    hardmap z2walkyky3zzv52g3gedrhxa665qdrslyvrljg5wppibx34olslnxgqd foo.com dev blogs dev.blogs [...]

### How it works

When you are setting up the mappings in a config file, you may have to
accomodate "subdomains"; the general form of a internet hostname is
like this:

* `hostname.domain.tld` # like: www.facebook.com or www.gov.uk
  * or: `hostname.domain.sld.tld` # like: www.amazon.co.uk
* `hostname.subdom.domain.tld` # like: www.prod.facebook.com
* `hostname.subsubdom.subdom.domain.tld` # cdn.lhr.eu.foo.net
* `hostname.subsubsubdom.subsubdom.subdom.domain.tld` # ...

... and so on, where:

* tld = [top level domain](https://en.wikipedia.org/wiki/Top-level_domain)
  * sld = [second level domain](https://en.wikipedia.org/wiki/.uk#Second-level_domains)
* domain = *generally the name of the organisation you are interested in*
* subdomain = *some kind of internal structure*
* hostname = *actual computer, or equivalent*

When you are setting up mappings, generally the rules are:

* you will **map one domain per onion**
* you will **ignore all hostnames**
* you will **append all possible subdomain stems**

So if your browser tells you that you are fetching content from
`cdn7.dublin.ireland.europe.foo.co.jp`, you should add a line like:

    hardmap %NEW_V3_ONION% foo.co.jp europe ireland.europe dublin.ireland.europe

... and Onionspray should do the rest. All this is necessary purely for
correctness of the self-signed SSL-Certificates - which are going to be weird,
anyway - and the rest of the HTML-rewriting code in Onionspray will be blind to
subdomains.

### Managing lots of sites and subdomains

Example:

* `www.foo.com.au`
* `www.syd.foo.com.au`
* `www.per.foo.com.au`,
* `www.cdn.foo.net`
* `www.foo.aws.amazon.com`
* ...

Put them all in the same project as separate mappings, remembering to
avoid the actual "hostnames" as described above:

    set project fooproj
    hardmap %NEW_V3_ONION% foo.com.au syd per
    hardmap %NEW_V3_ONION% foo.net cdn
    hardmap %NEW_V3_ONION% foo.aws.amazon.com

Onion mapping/translations will be applied for all sites in the same project.

## System startup and housekeeping procedures

Once you have installed Onionspray and configured and tested it for your
project, run:

* `./onionspray make-scripts`

This will create two files:

* `./onionspray-init.sh`: for installing on your system as a startup script
* `./onionspray-housekeeping.sh`: for cronjob log rotation and other cleanup
  work

Please read the individual files for installation instructions; local setup is
intended to be pretty simple.
