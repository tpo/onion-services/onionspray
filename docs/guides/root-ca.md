# Adding a Root CA Certificate to Tor Browser

**This is for development purposes only, you should not do this if you rely
upon the anonymity of Tor.**

With recent updates to Tor Browser (as of 20th June 2019), the storage of local
certificate roots is somewhat more complex, but can be performed as follows.

Be aware: whilst you are configured like this, some privacy-protective aspects
of Tor Browser are reduced or switched-off; ensure that you restore your browser
to defaults before using it for privacy-protecting purposes.

## Installation Part 1

* Open about:config.
* Click "I accept the risk!".
* Search for "security.nocertdb" in the box provided.
    * if the "value" field says "default"/"true", then:
    * double-click on it to make it "modified"/"false".
* Search for "browser.privatebrowsing.autostart" in the box provided.
    * if the "value" field says "default"/"true", then:
    * double-click on it to make it "modified"/"false".
* Search for "security.ssl.enable_ocsp_stapling" in the box provided.
    * if the "value" field says "default"/"true", then:
    * double-click on it to make it "modified"/"false".
* Dismiss the about:config tab.
* **IMPORTANT**: restart Tor Browser.

## Installation Part 2

* Open Menu > Tor Browser > Preferences > Privacy & Security.
* Scroll down to Security > Certificates.
* Uncheck Query OCSP responder servers to confirm the current validity of certificates.
* Click "View Certificates".
* Select "Authorities" tab.
* Click "Import", select your "rootCA.pem" file, click "Open".
* Popup: ensure that "Trust this CA to identify websites" is ticked/enabled.
* Click "Ok".
* Check that "mkcert development CA" now appears in the list of authorities.
* Navigate to the target URL.
* You have to disable OCSP too (as of 3th July 2019).

## Uninstalling

When you are eventually finished with your certificate:

* Uninstall/remove the certificate, using the same dialogues.
* Reverse the about:config changes which you performed above.
* **IMPORTANT**: restart Tor Browser.
