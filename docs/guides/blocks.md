# Building blocks

Onionspray has the following building blocks:

* **Server**: or a "machine", is any computer where Onionspray runs -- and you can
  run it in multiple servers.
* **Repository**: the [Onionspray repository][].
* **Installation**: a working copy of the repository, used to host Onion Service proxies.
* **Site**: a single Onion Service site that maps a DNS-based domain such as
  `example.org` (an optionally all it's subdomains) into an .onion address.
* **Project**: a project set of sites; each project has it's own instances of [C Tor][]
  (to run the Onion Services) and [OpenResty][] (for the reverse rewriting proxy).

A server can host many Onionspray installations, each one with many projects,
and each project with many sites.

[Onionspray repository]: https://gitlab.torproject.org/tpo/onion-services/onionspray
[C Tor]: https://gitlab.torproject.org/tpo/core/tor
[OpenResty]: http://openresty.org
